<?xml version="1.0" encoding="US-ASCII"?>
<!DOCTYPE rfc SYSTEM "rfc2629.dtd" [
<!ENTITY rfc2119 PUBLIC "" "http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.2119.xml">
<!ENTITY rfc5086 PUBLIC "" "http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.5086.xml">
<!ENTITY rfc5087 PUBLIC "" "http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.5087.xml">
<!ENTITY rfc3031 PUBLIC "" "http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.3031.xml">
<!ENTITY rfc3985 PUBLIC "" "http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.3985.xml">
<!ENTITY rfc7167 PUBLIC "" "http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.7167.xml">
<!ENTITY rfc4446 PUBLIC "" "http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.4446.xml">
<!ENTITY rfc4447 PUBLIC "" "http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.4447.xml">
<!ENTITY rfc4385 PUBLIC "" "http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.4385.xml">
<!ENTITY rfc4448 PUBLIC "" "http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.4448.xml">
<!ENTITY rfc6073 PUBLIC "" "http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.6073.xml">

<!ENTITY I-D.finn-detnet-problem-statement PUBLIC "" "http://xml2rfc.tools.ietf.org/public/rfc/bibxml3/reference.I-D.draft-finn-detnet-problem-statement-03.xml">
<!ENTITY I-D.finn-detnet-architecture PUBLIC "" "http://xml2rfc.tools.ietf.org/public/rfc/bibxml3/reference.I-D.draft-finn-detnet-architecture-01.xml">

]>

<?xml-stylesheet type='text/xsl' href='rfc2629.xslt' ?>
<?rfc toc="yes"?>
<?rfc symrefs="yes"?>
<?rfc sortrefs="yes"?>
<?rfc iprnotified="no"?>
<?rfc strict="yes"?>
<?rfc compact="yes"?>
<?rfc subcompact="yes"?>
<rfc category="std"
     docName="draft-korhonen-detnet-upwe-00"
	 ipr="trust200902"
	 submissionType="IETF">
  <front>
    <title abbrev="DetNet Data Plane">PseudoWire Data Plane for DetNet</title>

  <author fullname="Jouni Korhonen" initials="J." surname="Korhonen">
   <organization abbrev="Broadcom Corporation">Broadcom Corporation</organization>
   <address>
    <postal>
     <street>3151 Zanker Road</street>
     <city>San Jose</city>
     <code>95134</code>
     <region>CA</region>
     <country>USA</country>
    </postal>
    <email>jouni.nospam@gmail.com</email>
   </address>
  </author>

  <author initials="N" surname="Finn" fullname="Norman Finn" >
	<organization abbrev="Cisco">
		Cisco Systems
	</organization>
	<address>
		<postal>
			<street>170 W Tasman Dr.</street>
			<city>San Jose</city>
			<code>95134</code>
			<region>California</region>
			<country>USA</country>
		</postal>
		<phone>+1 408 526 4495</phone>
		<email>nfinn@cisco.com</email>
	</address>
  </author>

  <date />
  <workgroup>DetNet</workgroup>

  <abstract>
  <t>There are three techniques to achieve the Quality
   of Service required by deterministic networks: zero congestion loss, pinned-down
   paths and packet replications and deletion. This document specifically addresses
   the latter two of these techniques and specifies a PseudoWire-based user plane
   transport solution for the seamless redundancy aspect of deterministic networks.
  </t>
  </abstract>

  
  </front>

 <middle>
 <section anchor="intro" title="Introduction">
  <t>Deterministic networking (DetNet) <xref target="I-D.finn-detnet-problem-statement"/>
     provides a capability to carry unicast or multicast data streams for real-time applications
     with extremely low data loss rates and known upper bound maximum latency
     <xref target="I-D.finn-detnet-architecture"/>. The deterministic networking Quality of
     Service (QoS) is expressed as 1) the minimum and maximum end-to-end latency from sender
     (talker) to receiver (listener), 2) probability of loss of a packet, 3) probability of
     loss of a packet in the event of the failure of a relay system or link. Only the
     worst-case values for the mentioned parameters are concerned.
  </t>
  <t>There are three techniques to achieve the QoS required by deterministic networks:
   <list style="symbols">
    <t>zero congestion loss,</t>
    <t>pinned-down paths,</t>
    <t>packet replications and deletion (Seamless Redundancy).</t>
   </list>
   This document specifically addresses the last one of these techniques and specifies
   a PseudoWire-based <xref target="RFC3985"/> data plane
   solution for deterministic networks. 
  </t>
  <t>The solution defines two encapsulation methods. First, the existing transport of Ethernet over
    PseudoWires in both raw and tagged modes <xref target="RFC4448"/>. Second, a transport of
    Ethernet over PseudoWires with support of Ethernet header removal for specific data streams.
  </t>
 </section>

 <section title="Terminology">
  <t>The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT",
   "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this
   document are to be interpreted as described in <xref
   target="RFC2119"></xref>.</t>
 </section>

 <section title="Network architecture">
  <t>
   The following figure shows an example of a network emplying packet replicaiton and deletion
   to increase the probability of packet delivery.  End system A sources a stream destined
   for end system H.  Relay system B 1) adds a sequence number to each packet, 2) replicates
   that packet, and 3) sends the packet out on two different interfaces, one marked with
   label 31, and one with label 26.  Relay system C, without touching the sequence numbers,
   replicates the packets of stream 31, transmitting packets on two interfaces, labeled
   15 and 26.  Relay systems D and E are shown simply relaying the stream's packets from
   interface to interface.  (Perhaps they are bridges.)
   Relay system F combines the two streams 26 into a single stream
   26, using the sequence numbers in the packets to discard packets already transmitted.
   Relay system G combines streams 15 and 26 into a single stream.  It 1) discards any duplicates,
   2) removes the added sequence numbers and labels, and 3) passes a single data stream on
   to end system H.
  </t>

<figure title="Seamless Redundancy flexible positioning" anchor="arch">
<artwork><![CDATA[
                          +-----+       +-----+
                          |Relay|       |Relay|
                          | sys |_______| sys |______
                         /| tem |       | tem |      \
+-----+   +-----+       / |     |\      |     |       \
| End |   |Relay|      /  |  C  | \     |  E  |        \+-----+   +-----+
| sys |   | sys |_____/   +-----+  \  15+-----+         |Relay|   | End |
| tem |___| tem |        _ _ _ _ _ _\ _ _ _ _ _ _ _ _   | sys |   | sys |
|     |   |     |_____  /         \  \             15\  | tem |___| tem |
|  A  |   |  B  |     \   +-----+  \  \ +-----+       \ |     |   |     |
+-----+   +-----+      \  |Relay| 26\  \|Relay|        /|  G  |   |  H  |
                  31 /  \ | sys |    V  | sys |       / +-----+   +-----+
     _ _ _ _ _ _ _ _/    \| tem |_______| tem |______/   \_ _ _ _ 
                    \     |     |       |     |          / 
                     \    |  D  |       |  F  |         /
                   26 \   +-----+       +-----+        / 26
                       \_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ /
                                      26               
]]></artwork>
</figure>

 </section>


<section title="Deterministic networking services">

 <section title="Ethernet tagged mode">
  <t>Follows the solution defined in <xref target="RFC4448"/> Section 4.1.
  </t>
 </section>

 <section title="Ethernet raw mode">
  <t>Follows the solution defined in <xref target="RFC4448"/> Section 4.2.
  </t>
 </section>

 <section title="Ethernet header removal mode">
  <t>While is is possible that the end and relay systems do require the simulated
   Ethernet connection provided by a oseudowire, this is not always the case.  Often,
   connectivity at the IP layer is all that is desired, and the Ethernet MAC addresses
   have no use.
  </t>
  <t>A new encapsulation mode that:
   <list style="symbols">
    <t>Removes payload Ethernet header Source address,</t>
    <t>Removes payload Ethernet header Destination address,</t>
    <t>Removes payload Ethernet header EthType/Tag.</t>
   </list>
   All the above values are known by the PseudoWire Provider Edge nodes that are
   capable of removing and adding correct values based on the identified data stream.
   Further to header removal the generic procedures for transporting Ethernet
   payload defined in  <xref target="RFC4448"/> Section 4.4. are applied.
   
  </t>
 </section>

 <section title="Generic procedures">
  <t>What is needed in addition to "General Procedures" discussed in
   <xref target="RFC4448"/> Section 4.4.
  </t>
  <section title="Stream identification">
   <t>As discussed in <xref target="IEEE8021CB"/> Clause 6, there are a number of
	   options for identifying packets belonging to a stream:
	<list style="symbols">
	 <t>IEEE Audio Video Bridging streams <xref target="IEEE8021BA"/> are identified by a
		 {VLAN, destination MAC address} pair.</t>
	 <t>If processed inside an end system, particular sockets can be
		 identified as blonging to particular streams.</t>
	 <t>The octuple consisting of the source and destination MAC address, the VLAN ID,
	  the source and destination IP addresses, next protocol, and source and destination
	  port numbers can be used.</t>
	 <t>The MPLS labels are clearly available as stream identifiers.</t>
    </list>
   </t>
  </section>
   <section title="Stream re-identification">
	<t>
	 The start or the end of a label switched path may or may not coincide with the
	 sequencing, replication, or deletion functions; a stream may use label switching
	 for part of its journey, and other methods such as <xref target="IEEE8021CB"/>
	 before and/or after.  In these cases, it may be necessary to transfer the
	 sequence number from one encapsulation format (e.g. pseudowire) to another (e.g.
	 an <xref target="IEEE8021CB">IEEE 802.1CB</xref> sequence tag).
	</t>
   </section>
   <section title="Packet sequentialization">
	<t>As described in <xref target="I-D.finn-detnet-architecture"/>, a DetNet stream has
	 exactly one source.  If DetNet seamless redundancy is employed, the packets of
	 a DetNet stream are sequence numbered exactl once, before the stream's packets are first
	 replicated (if they are, in fact, replicated).  After that point, the sequence numbers
	 are copied, until they are removed from the packet.
	</t>
	<t>The procedures for generating packet sequence numbers described in <xref target="RFC4385"/>
	 are used for DetNet seamless redundancy streams with the following modification:
	 <list style="symbols">
	  <t>The value 0 SHALL NOT be sent in the Sequence Number field. (Seamless redundancy
	   requires a sequence number.)</t>
	 </list>
	</t>
   </section>
  </section>

 <section title="The Control Word" anchor="controlword">
  <t>The format of the Control Word is specified in <xref target="RFC4385"/>, and shown
   in the following figure.
  </t>
<figure title="Control word for deterministic networkind data streams" anchor="figcw">
<artwork><![CDATA[
 0                   1                   2                   3
 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|0 0 0 0| Flags |FRG|  Length   | Sequence Number               |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
]]></artwork>
</figure>
  <t>In a DetNet pseudowire Control Word, the Flags and FRG bits SHALL be sent as 0; fragmentation
   supported, and no Flags are defined.  The use of the Length field is as defined in
   <xref target="RFC4385"/>.
  </t>
 </section>

 <section title="Packet replication and deletion">
  <t>In order to support seamless redundancy service <xref target="IEEE8021CB"/>
   there has be a data stream split, a packet duplicate detection
   and deletion (sequence number recovery), and a stream merge function located at the
   PseudoWire Terminating Provided Edge (T-PE). The pseudoWire architecture document
   RFC3985 Section 5.2.1.2. already discusses frame duplicate detection.  However, to
   obtain a benefit in delivery reliablity commensurate with the cost of sending a
   stream over multiple paths simultaneously, a somewhat
   more sophisticated duplicate deleting algorithm is required.
  </t>
  <t>In same cases there is a need to further split the data stream between the
   talker and the listener(s). For this to work, the PseudoWire switching (or
   multi-segment) PseudoWire) <xref target="RFC6073"/> functionality is also needed. In
   the contect of seamless redundancy the Switching Provider Edge (S-PE) has to
   implement the data stream split, packet packet duplicate detections and
   deletion, and a stream merge function defined in this document.
  </t>
  <t>Describe data stream duplication -> make a reference to existing protection
   mechanisms and leverage those. [NWF: what is available in IETF]
  </t>
  <t>At this writing, a sequence generation and recovery algorithms have been proposed in
   <xref target="IEEE8021CB"/>.  Although they allow sequence number 0 to be used in a
   packet, a sequence number generation function that skips over 0 (as required by
   <xref target="RFC4385"/>) will not confuse the sequence recovery (packet deletion)
   function proposed in<xref target="IEEE8021CB"/>.  The propsed function invokes an
   instance of the sequence recovery function on each stream separately, then on the
   combined streams, in order to mitigate classes of failures (e.g. the repeatitive
   transmission of the same packet) that are observed in the field.
  </t>

 </section>


</section>

<section title="Management and operational considerations">
 <section title="LDP extensions">
  <t>
   <xref target="RFC4447"/>
   <xref target="RFC4448"/>
  </t>
 </section>

 <section title="Other management solutions">
  <t>Talk about Netconf and YANG here.
  </t>
 </section>
</section>




  <section title="Security considerations">
   <t>Data streams employing DetNet seamless redundancy are, for the most part,
	subject to the same security concerns as any other data stream.  The configuring
	of packet sequencing, packet replication, and packet deletion functions offers
	obvious additional points of attack.  Of these, only the packet deletion function
	is at all novel.
   </t>
   <t>If an attacker can subvert one of of the paths taken by a stream, it can supply
	those packets with sequence number values that will prevent the (presumably
	uncorrupted) packets on the other path(s) from reaching the ultimate destination.
	This points out that:
	<list style="symbol">
	 <t>Establishing an unterrupted chain of trusted relay systems along all paths of
	  a stream can convert a man-in-the-middle attack into a failure mode that is
	  mitigated by DetNet seamless redundancy.  (The break in trust becomes a failure
	  to propagate the bad data.)
	 </t>
	 <t>DetNet seamless redundancy is not a substitute for, and does not conflict with,
	  end-to-end security from original source to final destination(s).
	 </t>
	 <t></t>
	</list>
   </t>
  </section>


  <section anchor="iana" title="IANA Considerations">
   <t>We need new PW Types for a) detnet pwe using raw/tagged mode eth and
    b) for header removal...
   </t>
  </section>

  <section anchor="acks" title="Acknowledgements">
      <t>The author(s) ACK and NACK.
	  </t>
  </section>
 </middle>

 <back>
  <references title="Normative References">
   &rfc2119;
   &rfc3985;
   &rfc4385;
   &rfc4448;
   &rfc6073;
  </references>
  <references title="Informative References">
   &I-D.finn-detnet-problem-statement;
   &I-D.finn-detnet-architecture;
   &rfc3031;
   &rfc4446;
   &rfc4447;
   &rfc7167;

      <reference anchor="TSNTG"
       target="http://www.IEEE802.org/1/pages/avbridges.html">
        <front>
          <title>IEEE 802.1 Time-Sensitive Networks Task Group</title>
          <author>
            <organization>IEEE Standards Association</organization>
          </author>
          <date year="2013" />
        </front>
      </reference>
   <reference anchor="IEEE8021CB"
     target="http://www.ieee802.org/1/files/private/cb-drafts/d2/802-1CB-d2-1.pdf">
    <front>
     <title>Draft Standard for Local and metropolitan area networks - Seamless Redundancy</title>
     <author initials="N. F." surname="Finn" fullname="Norman Finn">
      <organization>IEEE 802.1</organization>
     </author>
     <date month="November" year="2015"/>
    </front>
    <seriesInfo name="IEEE P802.1CB /D2.1" value="P802.1CB"/>
    <format type="PDF" target="http://www.ieee802.org/1/files/private/cb-drafts/d2/802-1CB-d2-1.pdf"/>
   </reference>
   <reference anchor="IEEE8021BA"
	 target="http://standards.ieee.org/getIEEE802/download/802.1BA-2011.pdf">
   <front>
    <title>AVB Systems (IEEE 802.1BA-2011)</title>
	<author>
	 <organization>IEEE</organization>
	</author>
	<date year="2011" />
   </front>
  </reference>

  </references>
 </back>
</rfc>

